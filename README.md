# cgenius-flatpak

Commander Genius Flatpak support

# Building

```
flatpak-builder build-dir org.flatpak.commandergenius.yml
```

# Installing it

```
flatpak-builder build-dir org.commandergenius.yml
```

# Running it

```
flatpak run org.commandergenius.cgenius 
```


